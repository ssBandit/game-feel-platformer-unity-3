﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public int health = 3;
    public GameObject particles;

    [Range(0, 1)] public float shakeForce = 0.2f;
    [Range(0, 1)] public float shakeTime = 0.5f;

    Animator anim;
    AudioSource sound;


    private void Start()
    {
        anim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Damage"))
        {
            health--;
            anim.SetTrigger("Flash");
        }

        if(health <= 0)
        {
            GameObject breakAudio = new GameObject("box audio", typeof(AudioSource));
            breakAudio.GetComponent<AudioSource>().clip = sound.clip;
            breakAudio.transform.position = transform.position;
            breakAudio.GetComponent<AudioSource>().Play();

            Destroy(breakAudio, sound.clip.length + 1);

            CameraShake.self.StartCoroutine(CameraShake.self.Shake(shakeForce, shakeTime));

            Instantiate(particles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
