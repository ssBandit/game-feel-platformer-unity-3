﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public static CameraShake self;

    Vector3 originalPos;

    void Start()
    {
        originalPos = transform.position;
        self = this;
    }

    public IEnumerator Shake(float force, float duration)
    {
        float startTime = Time.time;

        while (Time.time - startTime <= duration)
        {
            float x = transform.position.x + Random.Range(-1.0f, 1.0f) * force;
            float y = transform.position.y + Random.Range(-1.0f, 1.0f) * force;

            transform.position = new Vector3(x, y, transform.position.z);

            yield return null;
        }

        transform.position = originalPos;
    }
}
