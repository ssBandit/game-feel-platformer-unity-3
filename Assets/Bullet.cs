﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 5;

    [Range(0, 1)] public float shakeForce = 0.2f;
    [Range(0, 1)] public float shakeTime = 0.5f;

    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        CameraShake.self.StartCoroutine(CameraShake.self.Shake(shakeForce, shakeTime));
        Invoke("Suicide", 5);
    }

    void Update()
    {
        rb.velocity = transform.right * speed;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Suicide();
    }

    void Suicide()
    {
        Destroy(gameObject);
    }
}
