﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed = 50;
    public float jumpForce = 20;
    public GameObject bullet;
    public float recoilForce = 10;
    public GameObject box;

    public AudioClip jumpSound;
    public AudioClip shootSound;

    Rigidbody2D rb;
    Animator anim;
    AudioSource sound;

    float originalPitch;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();

        originalPitch = sound.pitch;
    }

    void Update()
    {
        float move = Input.GetAxis("Horizontal") * speed;

        rb.velocity = new Vector2(move, rb.velocity.y);

        if(Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector2.up * jumpForce);
            anim.SetTrigger("Jump");
            sound.PlayOneShot(jumpSound);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            //Play sound
            sound.pitch = originalPitch;
            sound.pitch = Random.Range(sound.pitch - 0.3f, sound.pitch + 0.3f);
            sound.PlayOneShot(shootSound);


            //Calculate angle
            Vector3 mousePos = Input.mousePosition;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(mousePos);

            Vector3 direction = worldPos - (transform.position + Vector3.up);

            float degrees = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            Instantiate(bullet, transform.position + Vector3.up , Quaternion.AngleAxis(degrees, Vector3.forward));

            rb.AddForce(-direction.normalized * recoilForce, ForceMode2D.Impulse);
        }

        if(Input.GetKeyDown(KeyCode.B))
        {
            Instantiate(box); 
        }
    }
}
